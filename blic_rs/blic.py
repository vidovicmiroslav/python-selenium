from selenium import webdriver
import time

# path to chrome webdriver
path_to_chromedriver = '../chromedriver/chromedriver'
# Chrome browser with the webdriver
browser = webdriver.Chrome(executable_path = path_to_chromedriver)


def add_comment_to_blic():
    """Add a comment to the latest news on blic.rs
    """
    url = 'http://www.blic.rs/vesti'
    browser.get(url)

    first_news = browser.find_element_by_id("home_main_article").click()

    browser.find_element_by_class_name("nForum_commentContent").click()
    form_text = browser.find_element_by_class_name("nForum_commentContent")
    form_text.send_keys("Test")

    time.sleep(3)

    form_signature = browser.find_element_by_class_name(
        "nForum_signature").click()
    form_signature = browser.find_element_by_class_name(
        "nForum_signature")
    form_signature.send_keys("Test")

    time.sleep(3)

    form_send = browser.find_element_by_class_name(
        "k_rightPlaceHolder").click()


